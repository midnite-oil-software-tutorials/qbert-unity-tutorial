using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    [SerializeField] GameObject _coilyPrefab;
    [SerializeField] GameObject[] _enemyPrefabs;
    [SerializeField] float _spawnDelay = 3f;

    List<GameObject> _enemies, _availableEnemies;
    Coily _coily;
    float _delay;

    int MaxEnemies => 1 + ScoreManager.Instance.Level;

    bool ShouldSpawnEnemy
    {
        get
        {
            if (!GameManager.Instance.IsPlaying || GameManager.Instance.EnemiesFrozen) return false;
            if (_enemies.Count >= MaxEnemies) return false;
            _delay -= Time.deltaTime;
            return _delay <= 0f;
        }
    }

    bool ShouldSpawnCoily => !_coily;

    void Awake()
    {
        _enemies = new List<GameObject>();
        _availableEnemies = new List<GameObject>();
    }

    void OnEnable()
    {
        GameManager.Instance.GameStateChanged.AddListener(OnGameStateChanged);
    }

    void OnDisable()
    {
        GameManager.Instance.GameStateChanged.AddListener(OnGameStateChanged);
    }

    void Update()
    {
        if (!ShouldSpawnEnemy) return;
        SpawnEnemy();
    }

    void OnGameStateChanged()
    {
        switch (GameManager.Instance.GameState)
        {
            case GameManager.GameStates.GameStarted:
                GetEnemiesAvailableForLevel();
                break;
            case GameManager.GameStates.RoundStarted:
                _delay = _spawnDelay;
                break;
            case GameManager.GameStates.QBertDied:
            case GameManager.GameStates.GameOver:
                DestroyAllEnemies();
                break;
            case GameManager.GameStates.LevelComplete:
                DestroyAllEnemies();
                GetEnemiesAvailableForLevel();
                break;            
        }
    }

    void DestroyAllEnemies()
    {
        if (_coily)
        {
            DestroyCoily();
        }

        foreach (var enemy in _enemies)
        {
            DestroyEnemy(enemy, 0f);
        }
        
        _enemies.Clear();
    }

    void SpawnEnemy()
    {
        _delay = _spawnDelay;
        if (ShouldSpawnCoily)
        {
            _coily = Instantiate(_coilyPrefab, transform).GetComponent<Coily>();
            _coily.EnemyDied.AddListener(OnCoilyDied);
            return;
        }

        var prefab = GetRandomEnemyPrefab();
        var enemy = Instantiate(prefab, transform);
        enemy.GetComponent<EnemyBase>().EnemyDied.AddListener(OnEnemyDied);
        _enemies.Add(enemy);
    }

    void OnEnemyDied(GameObject enemy)
    {
        Debug.Log($"{enemy.name} died.");
        _enemies.Remove(enemy);
        DestroyEnemy(enemy, 2f);
    }

    GameObject GetRandomEnemyPrefab()
    {
        return _availableEnemies[Random.Range(0, _availableEnemies.Count)];
    }

    void OnCoilyDied(GameObject enemy)
    {
        Debug.Log($"{enemy.name} died.");
        DestroyAllEnemies();
    }

    void DestroyCoily()
    {
        if (!_coily) return;
        _coily.EnemyDied.RemoveListener(OnCoilyDied);
        Destroy(_coily.gameObject, 2f);
        _coily = null;
        _delay = _spawnDelay;
    }

    void DestroyEnemy(GameObject enemy, float delay = 2f)
    {
        if (!enemy) return;
        enemy.GetComponent<EnemyBase>()?.EnemyDied.RemoveListener(OnEnemyDied);
        Destroy(enemy, delay);
    }

    void GetEnemiesAvailableForLevel()
    {
        _availableEnemies.Clear();
        foreach (var enemyPrefab in _enemyPrefabs)
        {
            var enemyBase = enemyPrefab.GetComponent<EnemyBase>();
            if (enemyBase.StartLevel <= ScoreManager.Instance.Level)
            {
                _availableEnemies.Add(enemyPrefab);
            }
        }
    }
}
