using DG.Tweening;
using UnityEngine;

public class Slick : EnemyBase
{
    protected override void OnCollisionEnter(Collision other)
    {
        ClearJumpOnCollision();
        if (Helpers.CollidedWithQBert(other, out var qBert))
        {
            // todo - play death sound & show effects
            GameObject go;
            (go = gameObject).SetActive(false);
            EnemyDied.Invoke(go);
            return;
        }

        if (Helpers.CollidedWithPlatform(other, out var platform))
        {
            platform.FlipPlatform(-1);
        }
    }

    protected override Vector3 GetRandomJumpLocation()
    {
        var newFacing = _transform.eulerAngles;
        var position = _transform.position;
        var locations = GameManager.Instance.BoardManager.LegalJumpLocations(
            _legalJumpLocations, position, false, false, true);
        var location = _legalJumpLocations[Random.Range(0, locations)];
        var offset = position - location;
        newFacing = offset.y switch
        {
            < 0 when offset.z < 0 => BoardManager.SouthWest,
            < 0 => BoardManager.SouthEast,
            > 0 when offset.z > 0 => BoardManager.NorthEast,
            > 0 => BoardManager.NorthWest,
            _ => newFacing
        };

        if (newFacing != _transform.eulerAngles)
        {
            _transform.DORotate(newFacing, 0.25f)
                .SetEase(Ease.Linear).SetAutoKill(true);
        }
        
        return location;
    }
}
