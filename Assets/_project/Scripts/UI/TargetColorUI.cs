using UnityEngine;

public class TargetColorUI : MonoBehaviour
{
    [SerializeField] Renderer _baseRenderer, _platformRenderer;

    public void SetPlatformColors(Color baseColor, Color targetColor)
    {
        _baseRenderer.material.color = baseColor;
        _platformRenderer.material.color = targetColor;
    }
}
