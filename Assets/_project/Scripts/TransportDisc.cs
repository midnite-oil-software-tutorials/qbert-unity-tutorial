using System;
using DG.Tweening;
using UnityEngine;

public class TransportDisc : MonoBehaviour
{
    [SerializeField] float _rotateSpeed = 500f;
    [SerializeField] Vector3 _destinationLeft, _destinationRight;

    Vector3 _destination;
    Transform _transform, _disc;
    QBert _qBert;
    
    public bool IsActive { get; private set; }
    public bool LeftDisc => _destination == _destinationLeft;

    bool ReachedDestination => Mathf.Approximately(0f, Vector3.Distance(_transform.position, _destination));

    void Awake()
    {
        _transform = transform;
        _disc = _transform.GetChild(0);
    }

    void Update()
    {
        if (!IsActive) return;
        _disc.Rotate(0f, _rotateSpeed * Time.deltaTime, 0f, Space.Self);
        if (!ReachedDestination) return;
        _qBert.JumpToPlatform();
        IsActive = false;
        gameObject.SetActive(false);
    }

    void OnCollisionEnter(Collision other)
    {
        if (!other.collider.gameObject.TryGetComponent(out _qBert)) return;
        _qBert.transform.SetParent(_transform);
        ActivateDisc();
        _qBert.LandedOnDisc(_transform.position.x < 0);
    }

    void ActivateDisc()
    {
        _destination = _transform.position.z > 10f ? _destinationRight : _destinationLeft;
        IsActive = true;
        ScoreManager.Instance.UseTransportDisc();
        _transform.DOMove(_destination, 2f)
            .SetEase(Ease.Linear)
            .SetAutoKill(true);
    }
}
