using DG.Tweening;
using UnityEngine;

public class QBert : MonoBehaviour
{
    [SerializeField] Animator _animator;
    [SerializeField] AudioClip _jumpSound, _landSound, _fallSound, _deathSound;
    [SerializeField] GameObject _speechBubble;

    static readonly int JumpTrigger = Animator.StringToHash("Jump");
    static readonly int LandTrigger = Animator.StringToHash("Land");
    bool _jumping = false, _dead = false;
    Transform _transform;
    Rigidbody _rigidbody;
    Vector3 _startPosition = Vector3.zero, _jumpOrigin = Vector3.zero, _landingPosition;

    BoardManager BoardManager => GameManager.Instance.BoardManager;
    public Vector3 LandingPosition => _landingPosition;

    public Transform Body { get; private set; }
    void Awake()
    {
        _transform = transform;
        Body = _transform.GetChild(0);
        _rigidbody = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (!GameManager.Instance.IsPlaying || _dead || _jumping) return;
        if (!ReceivedPlayerInput(out var desiredFacing)) return;

        ChangeFacing(desiredFacing);
        
        if (BoardManager.LandingOutOfBounds(_landingPosition, Vector3.down))
        {
            if (JumpedToDisc())
            {
                return;
            }

            JumpOffEdge();
            return;
        }

        PerformJump(_landingPosition);
    }

    void OnCollisionEnter(Collision other)
    {
        if (!_jumping || _dead) return;
        TriggerLandingAnimation();
        if (!Helpers.CollidedWithPlatform(other, out var platform)) return;
        _jumping = false;
        if (!platform.Flipped)
        {
            platform.FlipPlatform(1);
            return;
        }

        PlayLandingSound();
    }

    void TriggerLandingAnimation()
    {
        _animator.SetBool(JumpTrigger, false);
        _animator.SetTrigger(LandTrigger);
    }

    public void ResetQBert(Vector3 qBertSpawnPosition, Vector3 qBertSpawnRotation)
    {
        _jumping = _dead = false;
        _animator.enabled = true;
        if (_startPosition == Vector3.zero)
        {
            _startPosition = qBertSpawnPosition;
        }
        _transform.position = qBertSpawnPosition;
        Body.eulerAngles = qBertSpawnRotation;
        _rigidbody.constraints = RigidbodyConstraints.FreezeAll;
        _rigidbody.velocity = _rigidbody.angularVelocity = Vector3.zero;
        gameObject.SetActive(true);
        _speechBubble.SetActive(false);
    }

    bool ReceivedPlayerInput(out Vector3 desiredFacing)
    {
        _landingPosition = _transform.position;
        desiredFacing = BoardManager.NoChange;
        
        // up and right (NorthEast)
        if (Input.GetKey(KeyCode.E))
        {
            desiredFacing = BoardManager.NorthEast;
            _landingPosition.y += 3;
            _landingPosition.z += 3;
        }
        
        // up and left (NorthWest)
        else if (Input.GetKey(KeyCode.Q))
        {
            desiredFacing = BoardManager.NorthWest;
            _landingPosition.y += 3;
            _landingPosition.x -= 3;
        }
        
        // Down and Right (SouthEast)
        else if (Input.GetKey(KeyCode.C))
        {
            desiredFacing = BoardManager.SouthEast;
            _landingPosition.y -= 3;
            _landingPosition.x += 3;
        }
        
        // Down and Left (SouthWest)
        else if (Input.GetKey(KeyCode.Z))
        {
            desiredFacing = BoardManager.SouthWest;
            _landingPosition.y -= 3;
            _landingPosition.z -= 3;
        }

        return _landingPosition != _transform.position;
    }

    void ChangeFacing(Vector3 desiredFacing)
    {
        if (desiredFacing == BoardManager.NoChange) return;
        Body.DORotate(desiredFacing, 0.25f).SetAutoKill();
    }
   
    bool JumpedToDisc()
    {
        Vector3 discPosition = _landingPosition;
        discPosition.y -= 3f;
        var disc = BoardManager.TransportDiscAtPosition(discPosition);
        if (!disc) return false;
        PerformJump(disc.transform.position);
        return true;
    }
    
    void JumpOffEdge()
    {
        if (_dead) return;
        MusicManager.Instance.Stop();
        SoundManager.Instance.PlayAudioClip(_fallSound, 1f);
        Vector3 fallPosition = _landingPosition;
        fallPosition.y = -10;
        if (_landingPosition.z > 20)
        {
            fallPosition.z = 30;
        }
        else if (_landingPosition.z < 0)
        {
            fallPosition.z = -10;
        }
        else if (_landingPosition.x < 0)
        {
            fallPosition.x = -10;
        }
        else if (_landingPosition.x > 18)
        {
            fallPosition.x = 28;
        }
        else
        {
            if (Body.eulerAngles == BoardManager.SouthEast)
            {
                fallPosition.x += 10;
            }
            else
            {
                fallPosition.z -= 10;
            }
        }
        _rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
        _transform.DOJump(fallPosition, 25, 1, 1).SetEase(Ease.InSine).SetAutoKill();
        _dead = true;
        GameManager.Instance.QBertDied();
    }
    
    void PerformJump(Vector3 landingPosition)
    {
        _jumping = true;
        _animator.SetBool(JumpTrigger, true);
        PlayJumpSound();
        _jumpOrigin = _transform.position;
        _transform.DOJump(landingPosition, 4, 1, 0.5f).SetEase(Ease.Linear).SetAutoKill();
    }

    void PlayJumpSound()
    {
        SoundManager.Instance.PlayAudioClip(_jumpSound);
    }

    public void LandedOnDisc(bool left)
    {
        TriggerLandingAnimation();
        ChangeFacing(left ? BoardManager.NorthEast : BoardManager.NorthWest);
    }

    void PlayLandingSound()
    {
        SoundManager.Instance.PlayAudioClip(_landSound);
    }

    public void JumpToPlatform()
    {
        _transform.SetParent(null);
        ChangeFacing(_transform.position.x < 0 ? BoardManager.SouthEast : BoardManager.SouthWest);
        PerformJump(_startPosition);
    }

    public void KillQBert()
    {
        if (_dead) return;
        _dead = true;
        _landingPosition = _jumpOrigin;
        MusicManager.Instance.Stop();
        SoundManager.Instance.PlayAudioClip(_deathSound);
        _animator.StopPlayback();
        _speechBubble.SetActive(true);
        GameManager.Instance.QBertDied();
    }
}
