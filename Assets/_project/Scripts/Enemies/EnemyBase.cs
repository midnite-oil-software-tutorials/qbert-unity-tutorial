using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

public class EnemyBase : MonoBehaviour
{
    [SerializeField] protected Animator _animator;
    [SerializeField] protected float _jumpDelay = 1f;
    [SerializeField] protected int _startLevel = 1;

    protected float _delay;
    protected Rigidbody _rigidbody;
    protected bool _jumping;
    protected bool _dead;
    protected Transform _transform;
    protected static readonly int Jumping = Animator.StringToHash("Jumping");
    protected static readonly int Landed = Animator.StringToHash("Landed");
    protected List<Vector3> _legalJumpLocations;

    public UnityEvent<GameObject> EnemyDied;
    public int StartLevel => _startLevel;

    protected bool CanJump
    {
        get
        {
            if (_jumping || _dead || GameManager.Instance.EnemiesFrozen) return false;
            _delay -= Time.deltaTime;
            return _delay <= 0f;
        }
    }

    protected void Awake()
    {
        _transform = transform;
        EnemyDied = new UnityEvent<GameObject>();
        _legalJumpLocations = new List<Vector3>();
        _rigidbody = GetComponent<Rigidbody>();
    }

    protected virtual void OnEnable()
    {
        GameManager.Instance.GameStateChanged.AddListener(OnGameStateChanged);
        _delay = _jumpDelay;
        _rigidbody.constraints = RigidbodyConstraints.FreezeAll;
    }

    protected virtual void OnDisable()
    {
        GameManager.Instance.GameStateChanged.RemoveListener(OnGameStateChanged);
    }

    void Update()
    {
        if (CanJump)
        {
            Jump();
        }
    }

    protected virtual void OnGameStateChanged()
    {
        if (GameManager.Instance.GameState is not 
            (GameManager.GameStates.LevelComplete or
             GameManager.GameStates.QBertDied)) return;
        gameObject.SetActive(false);
    }

    protected virtual void Jump()
    {
        _jumping = true;
        _delay = _jumpDelay;
        _animator.SetBool(Jumping, true);
        Vector3 location = GetRandomJumpLocation();
        if (BoardManager.LandingOutOfBounds(location, Vector3.down))
        {
            _dead = true;
            ScoreManager.Instance.AddScore(500);
            location = LocationOffEdge(location);
            _rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
            transform.DOJump(location, 25, 1, 1)
                .SetEase(Ease.InSine)
                .SetAutoKill(true);
            EnemyDied.Invoke(gameObject);
            return;
        }

        transform.DOJump(location, 4, 1, 0.5f)
            .SetEase(Ease.Linear)
            .SetAutoKill(true);
    }

    protected virtual Vector3 LocationOffEdge(Vector3 location)
    {
        Vector3 offEdgeLocation = location;
        offEdgeLocation.y -= 10;

        if (location.z > 20)
        {
            offEdgeLocation.z = 30;
        }
        else if (location.z < 0)
        {
            offEdgeLocation.z = 10;
        }
        else if (location.x < 0)
        {
            offEdgeLocation.x = -10;
        }
        else if (location.x > 18)
        {
            offEdgeLocation.x = 28;
        }
        else
        {
            if (_transform.eulerAngles == BoardManager.SouthEast)
            {
                offEdgeLocation.x += 10;
            }
            else
            {
                offEdgeLocation.z -= 10;
            }
        }
        return offEdgeLocation;
    }

    protected virtual Vector3 GetRandomJumpLocation()
    {
        Vector3 position = _transform.position;
        Vector3 facing = _transform.eulerAngles;

        transform.DORotate(facing, 0.25f)
            .SetEase(Ease.Linear)
            .SetAutoKill();

        return position;
    }

    protected virtual void OnCollisionEnter(Collision other)
    {
        ClearJumpOnCollision();
        
        if (!Helpers.CollidedWithQBert(other, out var qBert)) return;
        
        _dead = true;
        qBert.KillQBert();
        EnemyDied.Invoke(gameObject);
    }

    protected virtual void ClearJumpOnCollision()
    {
        if (!_jumping) return;
        _jumping = false;
        _animator.SetBool(Jumping, false);
        _animator.SetTrigger(Landed);
        _delay = _jumpDelay;
    }
}
