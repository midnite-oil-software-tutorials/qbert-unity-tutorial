
using System.Collections.Generic;
using UnityEngine;

public interface IPlatform
{
    bool Flipped { get; }
    void FlipPlatform(int direction);
    void ResetPlatform(List<Color> platformColors);
    void SetPlatformColor(Color color);
}
