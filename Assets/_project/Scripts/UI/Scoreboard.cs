using TMPro;
using UnityEngine;

public class Scoreboard : MonoBehaviour
{
    [SerializeField] GameObject[] _playerLives;
    [SerializeField] TMP_Text _levelText, _scoreText, _highScoreText;
    [SerializeField] TMP_Text _playAgainText, _gameOverText;
    [SerializeField] TMP_Text _levelCompleteText, _bonusPointsText;
    [SerializeField] TargetColorUI _targetColorUI;

    void OnEnable()
    {
        ScoreManager.Instance.ScoreChanged.AddListener(OnScoreChanged);
        GameManager.Instance.GameStateChanged.AddListener(OnGameStateChanged);
        GameManager.Instance.LivesChanged.AddListener(OnLivesChanged);
    }

    void OnDisable()
    {
        ScoreManager.Instance.ScoreChanged.RemoveListener(OnScoreChanged);
        GameManager.Instance.GameStateChanged.RemoveListener(OnGameStateChanged);
        GameManager.Instance.LivesChanged.RemoveListener(OnLivesChanged);
    }

    void OnGameStateChanged()
    {
        var boardManager = GameManager.Instance.BoardManager;
        
        switch (GameManager.Instance.GameState)
        {
            case GameManager.GameStates.Ready:
                _playAgainText.gameObject.SetActive(true);
                _gameOverText.gameObject.SetActive(false);
                _levelCompleteText.gameObject.SetActive(false);
                _levelText.gameObject.SetActive(false);
                _bonusPointsText.gameObject.SetActive(false);
                _targetColorUI.gameObject.SetActive(false);
                break;
            case GameManager.GameStates.GameStarted:
                _playAgainText.gameObject.SetActive(false);
                _gameOverText.gameObject.SetActive(false);
                _levelCompleteText.gameObject.SetActive(false);
                _levelText.gameObject.SetActive(false);
                _bonusPointsText.gameObject.SetActive(false);
                _targetColorUI.SetPlatformColors(boardManager.BaseColor, boardManager.TargetColor);
                _targetColorUI.gameObject.SetActive(true);
                break;
            case GameManager.GameStates.GameOver:
                _playAgainText.gameObject.SetActive(false);
                _gameOverText.gameObject.SetActive(true);
                _levelCompleteText.gameObject.SetActive(false);
                _levelText.gameObject.SetActive(false);
                _bonusPointsText.gameObject.SetActive(false);
                _targetColorUI.gameObject.SetActive(true);
                break;
            case GameManager.GameStates.LevelComplete:
                _playAgainText.gameObject.SetActive(false);
                _gameOverText.gameObject.SetActive(false);
                _levelCompleteText.text = $"Level {ScoreManager.Instance.Level - 1} Complete";
                _levelCompleteText.gameObject.SetActive(true);
                _levelText.gameObject.SetActive(false);
                _bonusPointsText.text = $"Bonus {ScoreManager.Instance.BonusPoints} points";
                _bonusPointsText.gameObject.SetActive(true);
                _targetColorUI.gameObject.SetActive(true);
                break;
            case GameManager.GameStates.RoundStarted:
            case GameManager.GameStates.QBertDied:
                _levelCompleteText.gameObject.SetActive(false);
                _playAgainText.gameObject.SetActive(false);
                _gameOverText.gameObject.SetActive(false);
                _levelText.text = $"Level {ScoreManager.Instance.Level}";
                _levelText.gameObject.SetActive(true);
                _bonusPointsText.gameObject.SetActive(false);
                _targetColorUI.SetPlatformColors(boardManager.BaseColor, boardManager.TargetColor);
                _targetColorUI.gameObject.SetActive(true);
                break;
            default:
                break;
        }
    }

    void OnLivesChanged()
    {
        for (int i = 0; i < _playerLives.Length; ++i)
        {
            _playerLives[i].SetActive(i < GameManager.Instance.Lives);
        }
    }

    void OnScoreChanged()
    {
        _levelText.text = $"Level {ScoreManager.Instance.Level}";
        _scoreText.text = ScoreManager.Instance.Score.ToString();
        _highScoreText.text = ScoreManager.Instance.HighScore.ToString();
    }
}
