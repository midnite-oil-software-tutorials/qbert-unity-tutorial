using UnityEngine;

public class FreezeBall : BadBall
{
    protected override void OnCollisionEnter(Collision other)
    {
        ClearJumpOnCollision();
        if (Helpers.CollidedWithQBert(other, out var qBert))
        {
            GameManager.Instance.FreezeEnemies();
            GameObject o;
            (o = gameObject).SetActive(false);
            EnemyDied.Invoke(o);
        }
    }
}
