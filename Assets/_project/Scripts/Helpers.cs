using UnityEngine;

public class Helpers
{
    public static bool CollidedWithPlatform(Collision other, out IPlatform platform)
    {
        return other.collider.gameObject.TryGetComponent<IPlatform>(out platform);
    }

    public static bool CollidedWithQBert(Collision other, out QBert qBert)
    {
        return (other.collider.TryGetComponent<QBert>(out qBert));
    }
}
