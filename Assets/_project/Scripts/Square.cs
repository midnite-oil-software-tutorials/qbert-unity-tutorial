using UnityEngine;

public class Square : MonoBehaviour
{
    [SerializeField] Renderer _renderer;
    
    public void SetBaseColor(Color baseColor)
    {
        _renderer.material.color = baseColor;
    }
}
