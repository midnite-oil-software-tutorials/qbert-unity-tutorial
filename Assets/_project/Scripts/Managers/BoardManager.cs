using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class BoardManager : MonoBehaviour
{
    [SerializeField] GameObject _squarePrefab;
    [SerializeField] TransportDisc _transportDiscPrefab;
    [SerializeField] Vector3[] _transportDiscPositions;
    [SerializeField] Color[] _baseColors, _platformColors;

    List<IPlatform> _platforms;
    List<Square> _platformBases;
    List<TransportDisc> _transportDiscs;
    Transform _transform;
    List<Vector3> _downDirections, _upDirections, _sideDirections;
    static LayerMask _platformLayer;

    public Color BaseColor { get; private set; }
    public Color TargetColor { get; private set; }
    
    static LayerMask PlatformLayer
    {
        get
        {
            if (_platformLayer == 0)
            {
                _platformLayer = LayerMask.GetMask("Platform");
            }

            return _platformLayer;
        }
    }

    void Awake()
    {
        _transform = transform;
        _transportDiscs = new List<TransportDisc>();
        _downDirections = new List<Vector3>
        {
            new Vector3(3, -3, 0),
            new Vector3(0, -3, -3)
        };
        _upDirections = new List<Vector3>
        {
            new Vector3(-3, 3, 0),
            new Vector3(0, 3, 3)
        };
        _sideDirections = new List<Vector3>
        {
            new Vector3(-3, 0, -3),
            new Vector3(3, 0, 3)
        };

    }

    public int UnFlippedPlatforms => _platforms.Count(p => !p.Flipped);
    public TransportDisc ActiveTransportDisc => _transportDiscs.FirstOrDefault(d => d.IsActive);

    public static readonly Vector3 NoChange = Vector3.up;
    public static readonly Vector3 NorthEast = Vector3.zero;
    public static readonly Vector3 NorthWest = new Vector3(0, 270, 0);
    public static readonly Vector3 SouthEast = new Vector3(0, 90,  0);
    public static readonly Vector3 SouthWest = new Vector3(0, 180, 0);

    public void SetUpBoard()
    {
        if (_transportDiscs.Count > 0)
        {
            ResetTransportDiscs();
        }
        else
        {
            CreateTransportDiscs();
        }

        if (_platforms?.Count > 0)
        {
            ResetPlatforms();
            return;
        }

        _platforms = new List<IPlatform>();
        _platformBases = new List<Square>();
        int blocksPerRow = 7;
        int y = 0;
        int startZ = 0;
        while (blocksPerRow > 0)
        {
            for (int i = 0; i < blocksPerRow; ++i)
            {
                GameObject square = Instantiate(_squarePrefab, _transform);
                square.transform.localPosition = new Vector3(i * 3, y, startZ + i * 3);
                _platforms.Add(square.GetComponentInChildren<IPlatform>());
                _platformBases.Add(square.GetComponent<Square>());
            }

            --blocksPerRow;
            startZ += 3;
            y += 3;
        }
        SetPlatformColors();
    }

    void ResetPlatforms()
    {
        SetPlatformColors();
    }

    void SetPlatformColors()
    {
        BaseColor = _baseColors[ScoreManager.Instance.Level - 1 % _baseColors.Length];
        foreach (var platformBase in _platformBases)
        {
            platformBase.SetBaseColor(BaseColor);
        }

        int platformLevels = 2 + Mathf.FloorToInt(ScoreManager.Instance.Level / 3f);
        List<Color> platformColors = new List<Color>();
        for (int i = 0; i < platformLevels; ++i)
        {
            int c = (ScoreManager.Instance.Level - 1 + i) % _platformColors.Length;
            platformColors.Add(_platformColors[c]);
            TargetColor = _platformColors[c];
        }

        foreach (var platform in _platforms)
        {
            platform.ResetPlatform(platformColors);
        }
    }

    public static bool LandingOutOfBounds(Vector3 landingPosition, Vector3 probeDirection)
    {
        var pos = landingPosition;
        pos.y += 0.5f;
        Debug.DrawRay(pos, probeDirection * 2f, Color.red, 5f);
        return !Physics.Raycast(pos, probeDirection, 2f, PlatformLayer);
    }

    public TransportDisc TransportDiscAtPosition(Vector3 position)
    {
        return _transportDiscs.FirstOrDefault(disc => Vector3.Distance(position, disc.transform.position) < 3f);
    }

    public void ShowVictoryEffect()
    {
        StartCoroutine(FlashPlatformColors(8.5f));
    }

    IEnumerator FlashPlatformColors(float duration)
    {
        while (duration > 0f)
        {
            duration -= Time.deltaTime;
            var color = Random.ColorHSV();
            foreach (var platform in _platforms)
            {
                platform.SetPlatformColor(color);
            }

            yield return null;
        }
    }

    void CreateTransportDiscs()
    {
        foreach (Vector3 discPosition in _transportDiscPositions)
        {
            var disc = Instantiate(_transportDiscPrefab, discPosition, Quaternion.identity)
                .GetComponent<TransportDisc>();
            _transportDiscs.Add(disc);
            disc.transform.SetParent(_transform);
        }
    }

    void ResetTransportDiscs()
    {
        for (var i = 0; i < _transportDiscPositions.Length; ++i)
        {
            _transportDiscs[i].transform.position = _transportDiscPositions[i];
            _transportDiscs[i].gameObject.SetActive(true);
        }
    }

    public int LegalJumpLocations(List<Vector3> legalJumpLocations, Vector3 position, bool downOnly = true, bool sideWalker = false, bool includeDown = false)
    {
        legalJumpLocations.Clear();

        if (downOnly || includeDown)
        {
            legalJumpLocations.AddRange(_downDirections.Select(vector => position + vector).Where(landingPosition => !LandingOutOfBounds(landingPosition, Vector3.down)));

            if (downOnly) return legalJumpLocations.Count;
        }

        legalJumpLocations.AddRange(_upDirections.Select(vector => position + vector).Where(landingPosition => sideWalker || !LandingOutOfBounds(landingPosition, Vector3.down)));

        if (sideWalker)
        {
            legalJumpLocations.AddRange(_sideDirections.Select(vector => position + vector));
        }

        return legalJumpLocations.Count;
    }
}