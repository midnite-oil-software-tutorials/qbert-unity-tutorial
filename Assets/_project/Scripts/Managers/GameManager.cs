using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    [SerializeField] GameObject _qBertPrefab;
    [SerializeField] Vector3 _qBertStartPosition, _qBertStartRotation;
    [SerializeField] AudioClip _victorySound, _gameOverSound;
    [SerializeField] int _extraLifeInterval = 8000;
    [SerializeField] float _enemyFreezeDelay = 5f;
    
    public enum GameStates
    {
        Ready,
        GameStarted,
        RoundStarted,
        QBertDied,
        LevelComplete,
        GameOver
    }
    GameStates _gameState;

    public GameStates GameState
    {
        get => _gameState;
        set
        {
            _gameState = value;
            GameStateChanged.Invoke();
        }
    }

    public bool EnemiesFrozen => _enemyFreezeCountdown > 0f;

    public bool IsPlaying => GameState == GameStates.RoundStarted;
    int _lives;

    public int Lives
    {
        get => _lives;
        private set
        {
            _lives = value;
            LivesChanged.Invoke();
        }
    }

    public UnityEvent LivesChanged = new UnityEvent();
    public UnityEvent GameStateChanged = new UnityEvent();
    public Transform QBert => _qBert.transform;
    public BoardManager BoardManager => _boardManager;

    Transform _transform;
    BoardManager _boardManager;
    QBert _qBert;
    Vector3 _qBertSpawnPosition, _qBertSpawnRotation;
    int _nextExtraLife;
    WaitForSeconds _threeSecondDelay, _nineSecondDelay;
    float _enemyFreezeCountdown;
    
    void Awake()
    {
        if (Instance != null && this != Instance)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;
        _transform = transform;
        _boardManager = GetComponent<BoardManager>();
        _threeSecondDelay = new WaitForSeconds(3f);
        _nineSecondDelay = new WaitForSeconds(9f);
    }

    void Start()
    {
        ScoreManager.Instance.ScoreChanged.AddListener(OnScoreChanged);
        ReadyToPlay();
    }

    void OnScoreChanged()
    {
        if (ScoreManager.Instance.Score >= _nextExtraLife)
        {
            ++Lives;
            _nextExtraLife += _extraLifeInterval;
        }
    }

    void Update()
    {
        _enemyFreezeCountdown -= Time.deltaTime;
        if (GameState != GameStates.Ready) return;
        if (Input.anyKeyDown)
        {
            StartGame();
        }
    }

    void StartGame()
    {
        GameState = GameStates.GameStarted;
        Lives = 3;
        _nextExtraLife = _extraLifeInterval; 
        StartRound();
    }

    void StartRound()
    {
        SpawnQBert();
        GameState = GameStates.RoundStarted;
        MusicManager.Instance.PlayGameMusic();
    }

    void SpawnQBert()
    {
        if (!_qBert)
        {
            _qBert = Instantiate(_qBertPrefab, _transform).GetComponent<QBert>();
        }

        _qBert.ResetQBert(_qBertSpawnPosition, _qBertSpawnRotation);
    }

    void ReadyToPlay()
    {
        MusicManager.Instance.PlayIntroMusic();
        Lives = 3;
        _nextExtraLife = _extraLifeInterval;
        ScoreManager.Instance.ResetScore();
        _boardManager.SetUpBoard();
        ResetQBertSpawnPosition();
        GameState = GameStates.Ready;
    }

    void ResetQBertSpawnPosition(bool useLastPosition = false)
    {
        if (useLastPosition)
        {
            _qBertSpawnPosition = _qBert.LandingPosition;
            _qBertSpawnRotation = _qBert.Body.eulerAngles;
            return;
        }

        _qBertSpawnPosition = _qBertStartPosition;
        _qBertSpawnRotation = _qBertStartRotation;
    }

    public void QBertDied()
    {
        --Lives;
        GameState = GameStates.QBertDied;
        StartCoroutine(HandleQBertDeath());
    }

    IEnumerator HandleQBertDeath()
    {
        yield return _threeSecondDelay;
        ResetQBertSpawnPosition(_qBert.transform.position.y > 0);
        _qBert.gameObject.SetActive(false);
        if (Lives > 0)
        {
            StartRound();
            yield break;
        }
        GameOver();
    }

    void GameOver()
    {
        SoundManager.Instance.PlayAudioClip(_gameOverSound);
        GameState = GameStates.GameOver;
        Invoke(nameof(ReadyToPlay), 3f);
    }

    public void PlatformFlipped()
    {
        if (_boardManager.UnFlippedPlatforms > 0) return;
        StartCoroutine(NextLevel());
    }

    IEnumerator NextLevel()
    {
        _qBert.gameObject.SetActive(false);
        ScoreManager.Instance.AddLevel();
        GameState = GameStates.LevelComplete;
        MusicManager.Instance.Stop();
        SoundManager.Instance.PlayAudioClip(_victorySound);
        _boardManager.ShowVictoryEffect();
        yield return _nineSecondDelay;
        _boardManager.SetUpBoard();
        ResetQBertSpawnPosition();
        StartRound();
    }

    public void FreezeEnemies()
    {
        _enemyFreezeCountdown = _enemyFreezeDelay;
    }
}