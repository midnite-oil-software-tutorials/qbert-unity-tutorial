using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour, IPlatform
{
    [SerializeField] Renderer _platformRenderer;
    [SerializeField] AudioClip _flipSound, _revertSound;

    readonly List<Color> _platformColors = new List<Color>();
    int _platformColor;

    public bool Flipped => _platformColor == _platformColors.Count - 1;
    
    public void FlipPlatform(int direction)
    {
        var newPlatformColor = Mathf.Clamp(_platformColor + direction, 0, _platformColors.Count - 1);
        if (newPlatformColor == _platformColor) return;
        _platformColor = newPlatformColor;
        SetPlatformColor(_platformColors[_platformColor]);
        SoundManager.Instance.PlayAudioClip(direction == 1 ? _flipSound : _revertSound, 0.25f);
        GameManager.Instance.PlatformFlipped();
        ScoreManager.Instance.AddScore(direction == 1 ? 25 : 0);
    }

    public void ResetPlatform(List<Color> platformColors)
    {
        _platformColors.Clear();
        _platformColors.AddRange(platformColors);
        _platformColor = 0;
        SetPlatformColor(_platformColors[0]);
    }
    
    public void SetPlatformColor(Color color)
    {
        _platformRenderer.material.color = color;
    }
}
