using UnityEngine;

public class BadBall : EnemyBase
{
    readonly Vector3 _outOfBounds = new(14f, -5f, 3f);
    protected override Vector3 GetRandomJumpLocation()
    {
        // Bad Ball always jumps down
        int locations = GameManager.Instance.BoardManager.LegalJumpLocations(
            _legalJumpLocations, _transform.position);
        return locations > 0 ? _legalJumpLocations[Random.Range(0, locations)] : _outOfBounds;
    }
}
