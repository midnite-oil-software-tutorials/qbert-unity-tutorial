using UnityEngine;

public class FaceCamera : MonoBehaviour
{
    Camera _mainCamera;
    Transform _transform;

    void Awake()
    {
        _transform = transform;
        _mainCamera = Camera.main;
    }

    void LateUpdate()
    {
        _transform.LookAt(_mainCamera.transform);
    }
}
